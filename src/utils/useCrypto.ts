import CryptoJS from 'crypto-js'

class UseCrypto {
  /**
   * 加密
   * @param data 加密源数据
   * @param key 加密标识
   * @return 返回 加密后的值
   */
  encrypt (data: any, key = 'DefChigga') {
    let msg = data + key
    return CryptoJS.SHA256(msg).toString()
  }
}

export const useCrypto = new UseCrypto()

/*
使用：
import { useCrypto } from '@/utils'

// 加密
let data1 = useCrypto.encrypt(loginForm.value.password, 'user') // data1为加密后的值
*/
