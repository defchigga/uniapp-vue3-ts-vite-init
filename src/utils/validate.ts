
export const mobileValidate = (value) => {
  if(!value) {
    uni.showToast({
      title: '请输入手机号',
      icon: 'none'
    });
    return false
  }
  const reg = /^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\d{8}$/
  if(!reg.test(value)) {
    uni.showToast({
      title: '手机号不正确',
      icon: 'none'
    });
    return false
  }
  return true
}

export const passwordValidate = (value) => {
  if(!value) {
    uni.showToast({
      title: '请输入密码',
      icon: 'none'
    });
    return false
  }
  return true
}

export const imgCodeValidate = (value) => {
  if(!value) {
    uni.showToast({
      title: '请输入图形验证码',
      icon: 'none'
    });
    return false
  }
  if(value.length !== 4) {
    uni.showToast({
      title: '图形验证码长度必须为4位',
      icon: 'none'
    });
    return false
  }
  return true
}

export const mobileCodeValidate = (value) => {
  if(!value) {
    uni.showToast({
      title: '请输入手机验证码',
      icon: 'none'
    });
    return false
  }
  if(value.length !== 6) {
    uni.showToast({
      title: '手机验证码长度必须为6位',
      icon: 'none'
    });
    return false
  }
  return true
}
