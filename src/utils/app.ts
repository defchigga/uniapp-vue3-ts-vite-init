// 监听网络
function onNetWorkState() {
	uni.getNetworkType({
		success: function(res) {
			if (res.networkType === 'none') {
				uni.showToast({
					title: '当前处于断网状态，请先连接网络',
					icon: 'none'
				});
			}
		}
	});
	uni.onNetworkStatusChange(function(res) {
		if (res.networkType === 'none') {
			uni.showToast({
				title: '当前处于断网状态，请先连接网络',
				icon: 'none'
			});
		} else if (res.networkType === '2g' || res.networkType === '3g') {
			uni.showToast({
				title: '当前网络信号弱，建议更换网络',
				icon: 'none'
			});
		}
	});
}

// APP更新
function onHotUpdate() {
	// #ifdef APP-PLUS  
	plus.runtime.getProperty(plus.runtime.appid, function(widgetInfo) {
		let platform = uni.getSystemInfoSync().platform
		let url = BASE_URL + '/api-admin/admin/itom/version/checkUpdate.do'
		console.log(url);
		uni.request({
			url: url, // 换成后台更新地址
			method: 'POST',
			data: {
				platform: [platform],
				version: widgetInfo.version,
			},
			success: (res) => {
				console.log([platform], widgetInfo.version);
				console.log('APP更新', res);
				// 存储APP版本号
				uni.setStorageSync('appVersion', widgetInfo.version)
				let data = res.data.data
				if(res.data.status === 1) {
					if(data) { // 需要更新
						let file = imgUrl + data.file[0].url
						let type = data.type
						console.log(type, file);
						switch (type){
							case 'apk': // 安卓整包
							uni.showModal({ //提醒用户更新
								title: "发现新版本",
								content: data.content,
								success: (resTmp) => {
									if (resTmp.confirm) {
										plus.runtime.openURL(file);
									}
								}
							})
								break;
							case 'ipa': // 苹果整包
							uni.showModal({ //提醒用户更新
								title: "发现新版本",
								content: data.content,
								success: (resTmp) => {
									if (resTmp.confirm) {
										plus.runtime.openURL(file);
									}
								}
							})
								break;
							case 'wgt': // 热更新
							// 热更新
							uni.downloadFile({
								url: file,
								success: (downloadResult) => {
									console.log(downloadResult);
									console.log(downloadResult.tempFilePath);
									if (downloadResult.statusCode === 200) {
										console.log('下载成功');
										plus.runtime.install(downloadResult.tempFilePath, {
												force: false
											},
											function() {
												console.log('安装成功');
												plus.runtime.restart();
											},
											function(e) {
												console.log(e)
												console.error('安装失败');
											});
									}
								}
							})
								break;
						}
					}else{
						/* uni.showToast({
							title: '已是最新版本',
							duration: 500
						}); */
						console.log('已是最新版本');
					}
				}else{
					uni.showToast({
						title: '获取更新包失败',
						icon: 'none'
					});
				}
			},
			complete(res) {
				console.log('app更新回调完成', res);
			}
		});
	});
	// #endif
}

// 监听获取缓存大小
function onStorageSize() {
	let res = uni.getStorageInfoSync()
	if (res.currentSize > 1024) {
		return (res.currentSize / 1024).toFixed(2) + 'M'
	} else {
		return res.currentSize + 'K'
	}
}

export {
	onNetWorkState,
	onHotUpdate,
	onStorageSize
}