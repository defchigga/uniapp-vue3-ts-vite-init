// 引入pinia创建方法createPinia
// https://pinia.vuejs.org/
import { createPinia } from 'pinia'

// 引入pinia持久化存储插件
// https://www.npmjs.com/package/pinia-plugin-persistedstate
import { createPersistedState } from 'pinia-plugin-persistedstate'

// 创建pinia
const pinia = createPinia()
// 持久化pinia
pinia.use(createPersistedState({
  storage: { // 改写 本地存储 storage
    getItem (key: string) {
      return uni.getStorageSync(key)
    },
    setItem (key: string, value: any) {
      uni.setStorageSync(key, value)
    }
  }
}))

export default pinia
