import { defineStore } from 'pinia'

export const useTemplateStore = defineStore('theme', {
  state: () => ({

  }),
  actions: {

  },
  persist: {
    key: 'theme',
    paths: ['xxx']
  }
})
