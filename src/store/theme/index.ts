import { defineStore } from 'pinia'

export const useThemeStore = defineStore('theme', {
  state: () => ({
    mainColor: '#FCD76C', // 主题色
    mainGradientColor: 'linear-gradient(90deg, #FCD86E 0%, #F5B93F 100%)', // 主题渐变
    subGradientColor: 'linear-gradient(90deg, #AE92CB 0%, #AA8CC8 22%, #9F7CC3 51%, #8E62B9 82%, #8351B3 100%)', // 次渐变
  }),
  actions: {
    // 设置主题色
    setMainColor(color) {
      this.mainColor = color
    }
  },
  persist: {
    key: 'theme',
    paths: ['mainColor', 'mainGradientColor', 'subGradientColor']
  }
})
