import { defineStore } from 'pinia'

export const useGlobalStore = defineStore('global', {
  state: () => ({
		globalData: '666'
  }),
  actions: {
    // 存储数据
    setGlobalDataAction () {
			this.globalData = 'DefChigga'
    },
   // 获取数据
   //getGlobalDataAction () {
	 //  console.log(this.globalData);
   //  }
  },
  persist: {
    key: 'global',
    paths: ['globalData']
  }
})
