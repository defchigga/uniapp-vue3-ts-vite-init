import { fetch } from '@/utils/http'

// 登录
export const requestUserLogin = (data: any) => {
	return fetch({
		url: '/api-admin/user/doLogin.do',
		method: 'POST',
		data
	})
}

// 获取用户信息
export const requestUserInfo = () => {
	return fetch({
		url: '/api-admin/user/getCurrentUser',
		method: 'POST'
	})
}

// 登出
export const requestUserLogout = () => {
	return fetch({
		url: '/api-admin/user/logout',
		method: 'POST'
	})
}
