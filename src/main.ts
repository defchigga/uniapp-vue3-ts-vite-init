import { createSSRApp } from 'vue'
import App from './App.vue'
import store from '@/store' // 引入pinia store

export function createApp () {
  const app = createSSRApp(App)
  app.use(store) // 注册pinia插件
  return {
    app
  }
}
